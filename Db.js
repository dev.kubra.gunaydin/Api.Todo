
var mongo = require("mongodb");
var url = "mongodb://dev.kubra:pass1234@ds163016.mlab.com:63016/devdb";
var Q = require("q");

var methods ={
    create :function(dataCreate){
        var deferred = Q.defer();
        mongo.connect(url , function(error , db){
            if (error){
                throw error;
            }
             
            db.collection("Todo").insertOne(dataCreate, (errCreate , collection) => {
                if(errCreate) throw errCreate;
                console.log("Record inserted successfully");
                db.close();
                console.log(collection)
                deferred.resolve(collection);
            });
        });
         return deferred.promise;  
    },
    list :  function(){
        var deferred = Q.defer();

        mongo.connect(url , function(error , db){
            if (error){
                throw error;
            }
             
                db.collection("Todo").find().toArray( (errList , collection) => {
                if(errList) throw errList;
                console.log("Record read successfully");
                console.log(collection);
                
                db.close();

                deferred.resolve(collection);
            });
        });

        return deferred.promise;
    },

    get :  function(id){
        var deferred = Q.defer();
        mongo.connect(url , function(error , db){
            if (error){
                throw error;
            }
             
            db.collection("Todo").findOne({taskId:id},(errGet , collection) => {
                if(errGet) throw errGet;
                console.log("Record read successfully");
                console.log(collection);
                db.close();
                deferred.resolve(collection);
            });
        });
        return deferred.promise;
    },
    delete : function(id){
        var deferred = Q.defer();
        mongo.connect(url, function(error, db){
            if(error) throw error;

            db.collection("Todo").deleteOne({taskId:id}, (errDelete,collection)=>{
                if(errDelete) throw errDelete;
                console.log(collection.result.n +  " " + "record deleted successfully");
                console.log(collection);
                db.close();
                deferred.resolve(collection);
            });
        });
        return deferred.promise;
    },
   update : function(id,dataUpdated){
       var deferred = Q.defer();
       mongo.connect(url, function(error,db){
           if(error) throw error;

           db.collection("Todo").updateOne({taskId:id}, {$set:dataUpdated}, (errUpdate,collection)=>{
               if(errUpdate) throw errUpdate;
               console.log("Record updated successfully");
               console.log(collection);
               db.close();
               deferred.resolve(collection);
           });
       }); 
       return deferred.promise;
   } 

};
module.exports = methods;


