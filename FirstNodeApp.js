var http = require('http');
var express = require('express');
var bodyParser = require('body-parser');
var db = require('./Db.js');
var app = express();
var cors = require('cors');

app.use(cors());
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());

app.listen(1212, function () {
    console.log("1212 portu dinleniyor");
});
app.post('/api/todo/save', function (request, response) {
    var todo = request.body;
    console.log(todo);
    var defer = db.create(todo);

    defer.then(function(result){
        if(result.insertedCount>0){
            response.send({
                message:"Your record was added successfully",
                status:true
            }); 
        }else{
            response.send({
                status:false
            });
        }
    });
    
});
app.get('/api/todo/list', function (request, response) {
    var defer = db.list();

    defer.then(function(res){
        if(res.length > 0){
            response.json({
                data:res,
                status:true
            });
        }else{
            response.json({
                status:false
            })
        }
        
    });
});
app.get('/api/todo/get/:id', function (request, response) {
    var defer = db.get(parseInt(request.params.id));

    defer.then(function(res){
        if(res){
            response.json({
                data:res,
                status:true
            });
        }else{
            response.json({
                status:false
            })
        }
    });
});

app.delete('/api/todo/delete/:id', function (request, response) {
    var defer = db.delete(parseInt(request.params.id))
    defer.then(function(res){
        if(res.result.n > 0){
            response.json({
                message:"Your record was deleted successfully",
                status:true
            });
        }else{
            response.json({
                status:false
            })
        }
    });
});

app.put('/api/todo/update/:id', function (request, response) {
    var data = request.body;
    var id = parseInt(request.params.id);
    var defer = db.update(id, data);
    defer.then(function(res){
        if(res.result.n > 0){
            response.json({
                message:"Your record was updated successfully",
                status:true
            });
        }else{
            response.json({
                status:false
            })
        }
    });
});